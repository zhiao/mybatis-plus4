package com.baomidou.mybatisplus.test.sqlserver.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 测试表test_user对应的实体类
 * @author sundongkai
 * @since 2022-04-03
 */
@Data
@Accessors(chain = true)
@TableName("test_user")
public class TestUser {

    private Integer id;
    private String name;
    private String phone;
}
