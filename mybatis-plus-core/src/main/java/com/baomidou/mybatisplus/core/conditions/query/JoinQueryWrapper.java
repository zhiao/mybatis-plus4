package com.baomidou.mybatisplus.core.conditions.query;

/**
 * 关联表的wrapper都要实现此接口
 * @author wanglei
 * @since 2022-03-14
 */
public interface JoinQueryWrapper<T> {
}
